import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.List;

import play.*;
import play.db.jpa.Blob;
import play.jobs.*;
import play.libs.MimeTypes;
import play.test.*;

import models.*;

@OnApplicationStart
public class Bootstrap extends Job 
{
	public void doJob() throws FileNotFoundException
	  {
	    if (User.count() == 0)
	    {
	      Fixtures.deleteDatabase();
	      Fixtures.loadModels("data.yml");
	    
	      loadPicture(User.findByEmail("homer@simpson.com"));
	      loadPicture(User.findByEmail("marge@simpson.com"));
	      loadPicture(User.findByEmail("barney@simpson.com"));
	      loadPicture(User.findByEmail("bart@simpson.com"));
	      loadPicture(User.findByEmail("maggie@simpson.com"));
	      loadPicture(User.findByEmail("lisa@simpson.com"));
	      loadPicture(User.findByEmail("ned@simpson.com"));
	      loadPicture(User.findByEmail("moe@simpson.com"));
	    }
	  }
	
	
	public void loadPicture(User user) throws FileNotFoundException
	{
		String photoName = "images/" + user.firstName + ".png";
		Blob blob = new Blob();
		blob.set(new FileInputStream(photoName), MimeTypes.getContentType(photoName));
		user.profilePicture = blob;
		user.save();		
	}
}
