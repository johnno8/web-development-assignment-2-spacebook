package controllers;


import java.util.*;
import play.*;
import play.mvc.*;
import utils.UserSocialComparator;
import utils.UserTalkativeComparator;

import models.*;


public class Leaderboard extends Controller
{
  public static void social()
  {
    List<User> users = User.findAll();
    Collections.sort(users, new UserSocialComparator());
    render(users);
  }
  
  public static void talkative()
  {
    List<User> users = User.findAll();
    Collections.sort(users, new UserTalkativeComparator());
    render(users);
  }
}
