package controllers;

import play.*;
import play.mvc.*;
import java.util.*;
import models.*;

public class Edit extends Controller
{
	public static void index()
	{
		if(session.get("logged_in_userid") == null)
		{
			Accounts.index();
		}
		else
		{
		  String userId = session.get("logged_in_userid");
		  User user = User.findById(Long.parseLong(userId));
		  render(user);
		}
	}
	
	public static void changeProfile(String firstName, String lastName, int age, String nationality, String email, String password)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		
		user.firstName = firstName;
		user.lastName = lastName;
		user.nationality = nationality;
		user.email = email;
		user.password = password;
		user.age = age;
		
		user.save();
		Logger.info("First name changed to: " + firstName);
		Profile.index();
	}
}
