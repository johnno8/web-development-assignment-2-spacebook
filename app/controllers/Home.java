package controllers;

import play.*;
import play.mvc.*;

import java.util.*;
import utils.MessageFromComparator;
import utils.MessageDateComparator;
import models.*;

public class Home extends Controller
{
	public static void index()
	{
		if(session.get("logged_in_userid") == null)
		{
			Accounts.index();
		}
		else
		{
			String userId = session.get("logged_in_userid");
			User user = User.findById(Long.parseLong(userId));
			render(user);
		}
	}

	public static void drop(Long id)
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));

		User friend = User.findById(id);

		user.unfriend(friend);
		Logger.info("Dropping" + friend.email);
		index();
	}

	public static void byUser()
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId));
		Collections.sort(user.inbox, new MessageFromComparator());

		for(Message m : user.inbox)
		{
			Logger.info(m.messageText);
		}

		render(user);
	}

	public static void byDate()
	{
		String userId = session.get("logged_in_userid");
		User user = User.findById(Long.parseLong(userId)); 
		Collections.sort(user.inbox, new MessageDateComparator());
		render(user);
	}

	public static void byConversation()
	{
	  String userId = session.get("logged_in_userid");
    User user = User.findById(Long.parseLong(userId));
    ArrayList<ArrayList<Message> > conversationList = new ArrayList<>();
    for (Friendship f : user.friendships)
    {
      conversationList.add(getConversation(user, f.targetUser));
    }
    render(user, conversationList);
	}
	
	static ArrayList<Message> getConversation(User user, User friend)
	{
		ArrayList<Message> conversation = new ArrayList<>();

		for (Message m : user.outbox)
		{
			if (m.to == friend)
			{
				conversation.add(m);
			}
		}

		for (Message m : user.inbox)
		{
			if (m.from == friend)
			{
				conversation.add(m);
			}
		}

		Collections.sort(conversation, new MessageDateComparator());
		return conversation;  
	}
}