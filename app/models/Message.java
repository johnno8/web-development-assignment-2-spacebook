package models;

import javax.persistence.*;
import play.db.jpa.*;
import java.util.ArrayList;
import java.util.Date;

@Entity
public class Message extends Model
{
	public String messageSubject;
	public String messageText;
	public Date postedAt;
	
	@ManyToOne
	public User from;
	
	@ManyToOne
	public User to;
	
	public Message(User from, User to, String messageSubject, String messageText)
	{
		this.from = from;
		this.to = to;
		this.messageText = messageText;
		this.messageSubject = messageSubject;
		this.postedAt = new Date();
	}
}
