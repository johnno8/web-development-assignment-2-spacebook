package utils;

import java.util.Comparator;
import models.Message;

public class MessageFromComparator implements Comparator<Message>
{
  public int compare(Message m1, Message m2)
  {
	String m1Name = m1.from.firstName + m1.from.lastName;
	String m2Name = m2.from.firstName + m2.from.lastName;
	return m1Name.compareTo(m2Name);
  }
}
